from django import forms
import datetime
# class NameForm(forms.Form):
#     your_name = forms.CharField(label='Your name', max_length=100)

class ChoiceForm(forms.Form):
    question = forms.IntegerField(widget=forms.HiddenInput())
    choice_text = forms.CharField(label='Your choice', max_length=1000)

class QuestionForm(forms.Form):
    question_text = forms.CharField(label='Your question', max_length=1000)
    pub_date = forms.DateTimeField(widget=forms.HiddenInput(), initial=datetime.datetime.now())