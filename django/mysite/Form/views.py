from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from myapp.models import Choice, Question
from .forms import QuestionForm, ChoiceForm

def index(request):
    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            Question(
                question_text = form.cleaned_data['question_text'],
                pub_date = form.cleaned_data['pub_date']
            ).save()
            return HttpResponseRedirect('/Form/')         
    form = QuestionForm()
    latest_question_list = Question.objects.order_by('-pub_date')
    context = {'latest_question_list': latest_question_list,"form": form}
    return render(request, 'Form/index.html', context)
   
    
def detail(request, question_id):
    if request.method == 'POST':
        form = ChoiceForm(request.POST)
        if form.is_valid():
            Choice(
                question = Question.objects.get(id = form.cleaned_data['question']), # пример избыточности
                choice_text = form.cleaned_data['choice_text']
            ).save()
            return HttpResponseRedirect(reverse("Form:detail", args=(question_id,)))  
    form = ChoiceForm(initial={"question": question_id})  
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'Form/detail.html', {'question': question, "form": form})


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'Form/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('Form:results', args=(question.id,)))


def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'Form/results.html', {'question': question})