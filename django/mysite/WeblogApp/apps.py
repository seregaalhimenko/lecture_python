from django.apps import AppConfig


class WeblogappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'WeblogApp'
