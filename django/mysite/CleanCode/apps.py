from django.apps import AppConfig


class CleancodeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'CleanCode'
